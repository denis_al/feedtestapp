//
//  CellModel.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation
import UIKit

struct FeedItemPresentationModel {
    let networkIcon: UIImage
    let date: String
    let avatarUrlString: String
    let accountName: String?
    let checkMarkImage: UIImage?
    let text: NSAttributedString?
    let attachmentInfo: AttachmentInfo?
    let authorName: String
    let webPageLink: String
}

struct AttachmentInfo {
    let url: String
    let size: CGSize
}
