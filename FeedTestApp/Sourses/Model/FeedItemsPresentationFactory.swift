//
//  CellModelBuilder.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation
import UIKit

final class FeedItemsPresentationFactory {
    
    func makeModel(with response:[FeedResponse]?) -> [FeedItemPresentationModel]? {
        let models = response?.compactMap({ (responseItem) -> FeedItemPresentationModel in
            let image = imageFor(responseItem.network)
            let date = dateFrom(responseItem.date)
            let text = hyperText(responseItem.text)
            let checkMarkImage = checkMark(responseItem.author.isVerified)
            let attacment = attacmentFor(responseItem.attachment)
            return FeedItemPresentationModel(networkIcon: image,
                                             date: date,
                                             avatarUrlString: responseItem.author.pictureLink,
                                             accountName: responseItem.author.account,
                                             checkMarkImage: checkMarkImage,
                                             text: text,
                                             attachmentInfo: attacment,
                                             authorName: responseItem.author.name,
                                             webPageLink: responseItem.link)
        })
        return models
    }
    
    private func imageFor(_ network: Networks) -> UIImage {
        return  UIImage(named: network.rawValue)!
    }
    
    private func checkMark(_ isAccountVerified: Bool) -> UIImage? {
        guard isAccountVerified else { return nil }
        return UIImage(named: "chek")
    }
    
    private func attacmentFor(_ image: Atachment?) -> AttachmentInfo? {
        guard let image = image else { return nil }
        return AttachmentInfo(url: image.pictureLink, size: CGSize(width: CGFloat(image.width), height: CGFloat(image.height)))
    }
    
    private func dateFrom(_ dateString: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = formatter.date(from: dateString)
        formatter.dateStyle = .full
        formatter.locale = Locale(identifier: "en_IE")
        var outputString = formatter.string(from: date!)
        guard let indexOfEmpty = outputString.firstIndex(of: " ") else { return outputString }
        outputString.insert(",", at: indexOfEmpty)
        return outputString
    }
    
    private func hyperText(_ textdata: HyperText?) -> NSAttributedString? {
        guard let textdata = textdata else { return nil }
        let atrString = NSMutableAttributedString(string: textdata.plain,
                                                  attributes: [.font: UIFont.systemFont(ofSize: 17),
                                                               .foregroundColor: UIColor.label
        ])
        textdata.markup.forEach { data in

            let from = textdata.plain.unicodeScalars.index(textdata.plain.unicodeScalars.startIndex, offsetBy: data.location)
            let to = textdata.plain.unicodeScalars.index(from, offsetBy: data.length)
            let fixedRange = NSRange(from..<to , in: textdata.plain)

            atrString.addAttributes([.link : data.link ], range: fixedRange)
        }
        return atrString
    }
}


