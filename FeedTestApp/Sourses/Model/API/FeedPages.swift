//
//  File.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation
import Moya

enum FeedPages: Int {
    case pageOne
    case pageTwo
}

extension FeedPages: TargetType {
    var baseURL: URL {
        return URL(string: "https://storage.googleapis.com")!
    }
    
    var path: String {
        switch self {
        case .pageOne:
            return "/cdn-og-test-api/test-task/social/1.json"
        case .pageTwo:
            return "/cdn-og-test-api/test-task/social/2.json"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    var validationType: ValidationType {
        return .successCodes
    }
}
