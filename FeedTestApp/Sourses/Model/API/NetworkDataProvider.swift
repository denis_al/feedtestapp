//
//  DataProvider.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation
import Moya

protocol DataProvider {
    func getDataFor(_ feed: FeedPages, completion:@escaping((Result<[FeedResponse]?, Error>) -> ()))
}

final class NetworkDataProvider: DataProvider {
    
    private let networkDataProvider = MoyaProvider<FeedPages>()
    
    func getDataFor(_ feed: FeedPages, completion:@escaping((Result<[FeedResponse]?, Error>) -> ())) {
        networkDataProvider.request(feed) { result in
            switch result {
            case .success(let response):
                do {
                    let posts = try response.map([FeedResponse].self)
                    completion(.success(posts))
                } catch  {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
