//
//  FeedResponse.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation

enum Networks: String, Codable {
    case twitter
    case facebook
    case instagram
}

struct FeedResponse: Codable {
    let network: Networks
    let link: String
    let date: String
    let author: Author
    let text:HyperText?
    let attachment: Atachment?
}

struct Author: Codable {
    let account: String?
    let isVerified: Bool
    let name: String
    let pictureLink: String
    
    enum CodingKeys: String, CodingKey {
        case pictureLink = "picture-link"
        case account
        case name
        case isVerified = "is-verified"
    }
}

struct Links: Codable {
    let location: Int
    let length: Int
    let link: String
}

struct HyperText: Codable {
    let markup: [Links]
    let plain: String
}

struct Atachment: Codable {
    let pictureLink: String
    let width: Int
    let height: Int
    
    enum CodingKeys: String, CodingKey {
        case pictureLink = "picture-link"
        case width
        case height
    }
}
