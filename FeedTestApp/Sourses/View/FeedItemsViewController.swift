//
//  TableViewController.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import UIKit

protocol FeedViewProtocol: class {
    func updateWith(_ dataSource: [FeedItemPresentationModel]?)
    func updateWith(_ dataSource: [FeedItemPresentationModel]?, indexes:[IndexPath])
    func showWebPageWith(_ request: URLRequest)
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func showAlertWith(_ error: Error?)
}

final class FeedItemsViewController: UITableViewController {
    
    private var spinner: UIView?
    private var presenter: FeedPresenterProtocol?
    private var dataSource: [FeedItemPresentationModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        setupRefreshControl()
        setupPresenterAndLoadData()
    }
    
    // MARK: - Table view data source & delegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedItemCell.reuseID, for: indexPath) as! FeedItemCell
        guard let dataSourse = dataSource else { return cell }
        let cellmodel = dataSourse[indexPath.row]
        cell.configureWith(cellmodel)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         guard let cellmodel = dataSource?[indexPath.row] else {
            self.showAlertWith(nil)
            return
        }
         presenter?.getDataForNextScreen(cellmodel)
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let dataSource = self.dataSource else { return }
        if indexPath.row == (dataSource.count - 1) {
            presenter?.loadData()
        }
    }
    
     // MARK: - Private
    
    private func setupPresenterAndLoadData() {
        presenter = FeedPresenter()
        presenter?.view = self
        presenter?.loadData()
    }
    
    private func setupTable() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName:"FeedItemCell", bundle: nil), forCellReuseIdentifier: FeedItemCell.reuseID)
    }
    
    private func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    @objc private func refresh() {
        presenter?.refresh()
    }
    
}

extension FeedItemsViewController: FeedViewProtocol {
    func updateWith(_ dataSource: [FeedItemPresentationModel]?, indexes: [IndexPath]) {
        guard let newItems = dataSource else { return }
        self.dataSource?.append(contentsOf: newItems)
        tableView.performBatchUpdates({
            tableView.beginUpdates()
            tableView.insertRows(at: indexes, with: .none)
            tableView.endUpdates()
        }, completion: nil)
    }
    
    func updateWith(_ dataSource: [FeedItemPresentationModel]?) {
            self.dataSource = dataSource
            tableView.reloadData()
            refreshControl?.endRefreshing()
    }
    
    func showWebPageWith(_ request: URLRequest) {
        let viewController = WebViewController(request: request)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showAlertWith(_ error: Error?) {
         let message = error?.localizedDescription ?? "An error occurred, Please try later"
               let alert = UIAlertController(title: "Error", message:message , preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
               self.present(alert, animated: true)
    }
    
    func showLoadingIndicator() {
        showSpinner(onView: view)
    }
    
    func hideLoadingIndicator() {
        removeSpinner()
    }
    
}

extension FeedItemsViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.startAnimating()
        ai.center = spinnerView.center
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        spinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.spinner?.removeFromSuperview()
            self.spinner = nil
        }
    }
}
