//
//  FeedScreenTableCell.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import UIKit
import Kingfisher

class FeedItemCell: UITableViewCell {
    
    static let reuseID = "FeedScreenTableCell"
    
    @IBOutlet private weak var avatarView: UIImageView!
    @IBOutlet private weak var checkmarkView: UIImageView!
    @IBOutlet private weak var accountNameLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var networkImageView: UIImageView!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var attacmentImageView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!

    private var imageAspectRatio: NSLayoutConstraint? {
        didSet {
            if let previousAspectRation = oldValue {
                previousAspectRation.isActive = false
                attacmentImageView.removeConstraint(previousAspectRation)
            }
            imageAspectRatio?.isActive = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.textContainer.lineFragmentPadding = 0
        textView.textContainerInset = .zero
        attacmentImageView.backgroundColor = .lightGray
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarView.image = nil
        checkmarkView.image = nil
        accountNameLabel.text = nil
        nameLabel.text = nil
        networkImageView.image = nil
        textView.text = nil
        attacmentImageView.image = nil
        dateLabel.text = nil
        imageAspectRatio = nil
        avatarView.kf.cancelDownloadTask()
        attacmentImageView.kf.cancelDownloadTask()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        avatarView.layer.cornerRadius = avatarView.bounds.midY
    }

    func configureWith(_ cellModel: FeedItemPresentationModel) {
        setupImafeFor(imageView: avatarView, url: cellModel.avatarUrlString)

        if let attachment = cellModel.attachmentInfo {
            attacmentImageView.isHidden = false
            setupAttachmentImageView(with: attachment)
        } else {
            attacmentImageView.isHidden = true
        }

        checkmarkView.image = cellModel.checkMarkImage
        accountNameLabel.text = cellModel.accountName
        nameLabel.text = cellModel.authorName
        networkImageView.image = cellModel.networkIcon
        textView.attributedText = cellModel.text
        dateLabel.text = cellModel.date
    }

    private func setupImafeFor(imageView: UIImageView, url: String) {
        let imageUrl = URL(string: url)
        let plaseHolder = UIImage(named: "avatar")
        imageView.kf.setImage(with: imageUrl, placeholder: plaseHolder)
    }
    
    private func setupAttachmentImageView(with attachment: AttachmentInfo) {
        imageAspectRatio = attacmentImageView.widthAnchor.constraint(
            equalTo: attacmentImageView.heightAnchor,
            multiplier: attachment.size.width / attachment.size.height)

        let processor = DownsamplingImageProcessor(size: attachment.size)
        let imageUrl = URL(string: attachment.url)
        attacmentImageView.kf.indicatorType = .activity
        let options: KingfisherOptionsInfo = [
            .processor(processor),
            .scaleFactor(UIScreen.main.scale),
            .transition(.fade(1)),
            .cacheOriginalImage ]
        attacmentImageView.kf.setImage(with: imageUrl, options: options)
    }
}
