//
//  WebViewController.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/8/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation
import UIKit
import WebKit


final class WebViewController: UIViewController {
    
    private var request: URLRequest
    private var webView: WKWebView!
    
    init(request: URLRequest) {
        self.request = request
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.load(request)
    }
}

extension WebViewController: WKUIDelegate {
    
}
