//
//  FeedPresenter.swift
//  FeedTestApp
//
//  Created by Alioshyn, Dzianis on 3/6/20.
//  Copyright © 2020 Alioshyn, Dzianis. All rights reserved.
//

import Foundation

protocol FeedPresenterProtocol {
    var view: FeedViewProtocol? {get set}
    func loadData()
    func refresh()
    func getDataForNextScreen(_ model: FeedItemPresentationModel)
}

final class FeedPresenter {
    weak var view: FeedViewProtocol?
    private var dataSource: [FeedItemPresentationModel]?
    private var loadedPage = 0
    private var dataProvider: DataProvider
    private var modelBuilder: FeedItemsPresentationFactory
    
    init(dataProvider:DataProvider = NetworkDataProvider(),
         modelBuilder: FeedItemsPresentationFactory = FeedItemsPresentationFactory()) {
        self.dataProvider = dataProvider
        self.modelBuilder = modelBuilder
    }
    
    private func loadDataFor(_ page: FeedPages) {
        
        dataProvider.getDataFor(page) { [weak self] response in
            guard let self = self else { return }
            self.view?.showLoadingIndicator()
            switch response {
            case .success(let posts):
                let models =  self.modelBuilder.makeModel(with: posts)
                if page != .pageOne {
                    let indexes = self.calculateIndexPathsToReload(from: models)
                    self.view?.updateWith(models, indexes: indexes)
                    self.dataSource = models
                } else {
                    self.view?.updateWith(models)
                    self.dataSource = models
                }
            case .failure(let error):
                self.view?.showAlertWith(error)
            }
            self.view?.hideLoadingIndicator()
        }
    }
    
    private func calculateIndexPathsToReload(from modelItems: [FeedItemPresentationModel]?) -> [IndexPath] {
        guard
            let loadedItems = dataSource,
            let newItems = modelItems
            else { return [] }
        let startIndex = loadedItems.count 
        let endIndex = startIndex + newItems.count
        return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
    }
}

extension FeedPresenter: FeedPresenterProtocol {
    func loadData() {
        switch  loadedPage {
        case 0:
            loadDataFor(.pageOne)
            loadedPage += 1
        default:
            guard
                loadedPage <= FeedPages.pageTwo.rawValue,
                let page = FeedPages(rawValue: loadedPage) else {
                    return
            }
            loadedPage += 1
            loadDataFor(page)
        }
        
        
    }
    
    func refresh() {
        loadDataFor(.pageOne)
        loadedPage = 1
    }
    
    func getDataForNextScreen(_ model: FeedItemPresentationModel) {
        guard  let url = URL(string: model.webPageLink)
        else { return }
        let request = URLRequest(url: url)
        view?.showWebPageWith(request)
    }
    
}
